﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EyeDetector
{
    public class DtoEllipsLimits
    {
        public byte hmin { get; set; }
        public byte hmax { get; set; }
        public byte smin { get; set; }
        public byte smax { get; set; }
        public byte vmin { get; set; }
        public byte vmax { get; set; }
    }
}

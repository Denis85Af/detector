﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EyeDetector
{
   public class ImageService
    {
        public void SetImagesLimits(string[] FileNames, int ItemsCount)
        {
            Core.SetImgOriginal(FileNames[0]);
            var core = new Core();
            ellips[] eye = new ellips[ItemsCount];
            ellips[] point = new ellips[ItemsCount];

            for (int i = 0; i < eye.Count(); i++)
            {
                //ограничиваем инициализацию 10 изображениями               
                eye[i] = new ellips();
                point[i] = new ellips();
                //записываем все изображения
                Core.SetImgOriginal(FileNames[i]);
                var limits = core.FndChannels();
                eye[i].X0 = 0;
                eye[i].Y0 = 0;
                eye[i].Width = 0;
                eye[i].Height = 0;
                point[i].X0 = 0;
                point[i].Y0 = 0;
                point[i].Width = 0;
                point[i].Height = 0;

                eye[i].Hmin = limits.hmin;
                eye[i].Hmax = limits.hmax;
                eye[i].Smin = limits.smin;
                eye[i].Smax = limits.smax;
                eye[i].Vmin = limits.vmin;
                eye[i].Vmax = limits.vmax;

                point[i].Hmin = limits.hmin;
                point[i].Hmax = limits.hmax;
                point[i].Smin = limits.smin;
                point[i].Smax = limits.smax;
                point[i].Vmin = limits.vmin;
                point[i].Vmax = limits.vmax;
            }
        }
    }
}
